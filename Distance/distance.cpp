#include"distance.h"
int d_feet,d_inches;
Distance::Distance():m_feet(0),m_inches(0){}
Distance::Distance(int ft,int inch):m_feet(ft),m_inches(inch){}
Distance::Distance(const Distance &ref):m_feet(ref.m_feet),m_inches(ref.m_inches){}
Distance Distance::operator+(const Distance &ref){
     d_feet=m_feet+ref.m_feet;
     d_inches=m_inches+ref.m_inches;
     check();
     return Distance (d_feet,d_inches);
}
Distance Distance::operator-(const Distance &ref){
     d_feet=m_feet-ref.m_feet;
     d_inches=m_inches-ref.m_inches;
     check();
     return Distance(d_feet,d_inches);
}
bool Distance::operator==(const Distance &ref){
    if(m_feet==ref.m_feet && m_inches==ref.m_inches)
        return true;
    else
        return false;
}
Distance Distance::operator+(int val){
        d_feet=m_feet;
        d_inches=m_inches+val;
        check();
        return Distance(d_feet,d_inches);
    }    
Distance Distance::operator++()
    {
        ++m_inches;
        check();
        return *this;
    }
bool Distance::operator<(const Distance &ref){
    return m_feet<ref.m_feet && m_inches<ref.m_inches;

}   
bool Distance::operator>(const Distance &ref){
    return m_feet>ref.m_feet && m_inches>ref.m_inches;

}    
void Distance::check()
{
    if(d_inches>=12){
		    d_feet += d_inches/12;
		    d_inches = (d_inches % 12);

    }
    else if(d_inches<0){
		    d_feet -= (abs(d_inches)/12 + 1);
		    d_inches = 12 - (abs(d_inches) % 12);
    }
}
std::ostream &operator<<(std::ostream &rout,const Distance&ref)
{
    rout<<ref.m_feet<<"."<<ref.m_inches<<"\n";
    return rout;
}          
    

