#include<iostream>
#include<string>
class Customer 
{
private:
	int m_custid;
	std::string m_name;
  	std::string m_phno;
public:
	Customer(int id,std::string name,std::string phno):m_custid(id),m_name(name),m_phno(phno){
  	}
  	virtual void display() const;
  	virtual void makeCall(int)=0;
  	virtual void credit(double){
  	}
  	virtual void billPay(double){
	}
  	virtual double getBalance()const{
	}
};


void Customer:: display() const
{
  	std::cout<<"Customer ID: "<<m_custid<<std::endl<<"Name:"<<m_name<<std::endl<<"Mobile number:"<<m_phno<<std::endl;
}

class PostpaidCustomer :public Customer
{
	double m_balance;
public:
  	PostpaidCustomer(int id,std::string name,std::string phno,double bal):Customer(id,name,phno),m_balance(bal){
  	}
  	PostpaidCustomer(int id,std::string name,std::string phno):Customer(id,name,phno),m_balance(0){
  	}
  	void makeCall(int);
  	double getBalance()const;
  	void billPay(double);
  	void display()const;  
};  

void  PostpaidCustomer::makeCall(int n)
{
	m_balance-=n;
}
double PostpaidCustomer::getBalance()const
{
	return m_balance;
}
void PostpaidCustomer::billPay(double amt)
{
	m_balance-=amt;
}
void PostpaidCustomer::display()const
{
	Customer::display();
	std::cout<<"Balance:"<<m_balance<<std::endl;
}


class PrepaidCustomer :public Customer
{
	double m_balance;
public:
  	PrepaidCustomer(int id,std::string name,std::string phno,double bal):Customer(id,name,phno),m_balance(bal){
  	}
  	PrepaidCustomer(int id,std::string name,std::string phno):Customer(id,name,phno),m_balance(100){
  	}
  	void makeCall(int);
  	double getBalance()const;
  	void credit(double);
  	void display()const;  
};  

void  PrepaidCustomer::makeCall(int n)
{
	m_balance-=n;
}
double PrepaidCustomer::getBalance()const
{
	return m_balance;
}
void PrepaidCustomer::credit(double amt)
{
	m_balance+=amt;
}
void PrepaidCustomer::display()const
{
	Customer::display();
	std::cout<<"Balance:"<<m_balance<<std::endl;
}

int main() {

  PostpaidCustomer pobj(1001, "Richard", "9845012345");
  pobj.display();
  Customer *ptr = new PostpaidCustomer(1001, "Scott", "98223 12345", 500);
  ptr->makeCall(5);    //should invike from derived class
  ptr->billPay(100);  //should invoke from derived class
  int bal = ptr->getBalance();  
  std::cout << "Balance is :" << bal << "\n";
  ptr->display();
  PrepaidCustomer nobj(1005, "Robin", "9845012335");
  nobj.display();
  Customer *ptr2 = new PrepaidCustomer(1003, "Meyers","9845012346",200 );
  ptr2->makeCall(5);    //should invike from derived class
  ptr2->credit(100);  //should invoke from derived class
  int bal2 = ptr2->getBalance();  
  std::cout << "Balance is :" << bal2 << "\n";
  ptr2->display();
  delete ptr;
}

