#include "iphead.h"

int main()
{
	IPAddress ip1;
	ip1.display();
	IPAddress ip2(192,78,12,45);
	ip2.display();
	IPAddress ip3("192.45.67.23");
	ip3.display();
	return 0;
}
