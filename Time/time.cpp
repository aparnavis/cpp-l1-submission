#include<iostream>
#include<iomanip>
#include"time1.h"
MyTime:: MyTime():m_hr(0),m_min(0),m_sec(0)
{
}

MyTime::MyTime(int hr,int min,int sec):m_hr(hr),m_min(min),m_sec(sec)
{
}

MyTime::MyTime(const MyTime &ref):m_hr(ref.m_hr),m_min(ref.m_min),m_sec(ref.m_sec)
{ 
}

MyTime overflow(int hour,int mins,int secs)
{
	if(secs>=60)
	{
		secs-=60;
		mins++;
	}
	if(mins>=60)
	{
		mins-=60;
		hour++;
	}
	
	return MyTime(hour,mins,secs);
}

MyTime MyTime::operator +(const MyTime& ref)const
{
	int hr,min,sec;
	sec=m_sec+ ref.m_sec;
	min=m_min+ref.m_min;
	hr=m_hr+ref.m_hr;
	return overflow(hr,min,sec);
} 

MyTime MyTime::operator +(const int& val)const
{
	int hr,min,sec;
	hr=m_hr;
	min=m_min;
	sec=m_sec+val;
	return overflow(hr,min,sec);
}

MyTime MyTime::operator ++()
{
	++m_sec;
	return *this;
}

bool MyTime::operator ==(const MyTime& ref)const
{
	if(m_hr==ref.m_hr&&m_min==ref.m_min&&m_sec==ref.m_sec)
		return true;
	return false;
}

void MyTime::display()const
{
	std::cout<<std::setfill('0')<<std::setw(2);
	std::cout<<m_hr<<":";
	std::cout<<std::setfill('0')<<std::setw(2);
	std::cout<<m_min<<":";
	std::cout<<std::setfill('0')<<std::setw(2);
	std::cout<<m_sec<<std::endl;
}