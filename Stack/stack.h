#include<iostream>
const int SIZE=10;
template<class T>
class Stack
{
    private:
        int m_tos;
        T stk[SIZE];
    public:
        Stack():m_tos(0){}
        void push(int);
        T pop();
        T peek();
};
template<class T>
void Stack<T>::push(int val){
    if(m_tos==SIZE)
        std::cout<<"Stack is full"<<"\n";
    else
    {
        stk[m_tos]=val;
        m_tos++;
    }
}
template<class T>
T Stack<T>::pop(){
    if(m_tos==0){
        std::cout<<"Stack is empty"<<"\n";
        return 0;
    }
    else
    {
        m_tos--;
        return stk[m_tos];
    }
}
template<class T>
T Stack<T>::peek(){
    if(m_tos==0){
        std::cout<<"Stack is empty"<<"\n";
        return 0;
    }
    else
    {
        return stk[m_tos];
    }
}    

