#include "currency.h"

int main() 
{
  Currency c1(10,20);
  Currency c2(18,10);
  Currency c3;         
  c3 = c1 + c2;
  c3.display();      
 
  Currency c4;
  c4 = c1 + 50; 
  c4.display();     

  ++c1; 
  c1.display();           

  (++c2).display();  
  c4 = c1;
  c4.display();
  
  Currency c5(c2);
  if(c5 == c2) std::cout <<"Equal"<< "\n";
  std::cout<<c5<<"\n";
  return 0;
}
