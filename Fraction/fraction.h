#include<iostream>
#include<fstream>
class Fraction{
    int m_numerator;
    int m_denominator;
    public:
        Fraction();
        Fraction(int,int);
        Fraction(const Fraction &ref);
        Fraction operator+(const Fraction &ref);
        Fraction operator-(const Fraction &ref);
        Fraction operator*(const Fraction &ref);
        Fraction operator+(int);
        Fraction operator*(int);
        bool operator==(const Fraction &ref);
        int gcd(int,int);
        friend std::ostream &operator<<(std::ostream &,const Fraction&); 

};