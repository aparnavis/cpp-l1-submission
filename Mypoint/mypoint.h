#include<iostream>
#include<math.h>
enum Quadrant { Q1 = 1, Q2 = 2, Q3 = 3, Q4 = 4 };
template <class Point>
class MyPoint {
    Point x;
    Point y;
public:
    MyPoint();
    MyPoint(Point x1, Point y1);
    Point distanceFromOrigin()const;
    Quadrant getQuadrant()const;
    void move(Point, Point);
    void display()const;
};


template <class Point>
MyPoint<Point>::MyPoint():x(1),y(1)
{
}

template <class Point>
MyPoint<Point>::MyPoint(Point x1, Point y1):x(x1),y(y1)
{
}


template <class Point>
Point MyPoint<Point>::distanceFromOrigin()const
{
    return sqrt(pow(x,2) + pow(y,2));
}

template <class Point>
Quadrant MyPoint<Point>::getQuadrant()const
{
    if (x > 0 && y > 0) return Q1;
	else if (x < 0 && y>0) return Q2;
    else if (x < 0 && y < 0) return Q3;
    else return Q4;
}

template <class Point>
void MyPoint<Point>::move(Point x1, Point y1)
{
    x = x + x1;
	y = y + y1;
}


template <class Point>
void MyPoint<Point>::display()const
{
    std::cout << "("<<x<<","<< y<<")" << std::endl;
}
 
   