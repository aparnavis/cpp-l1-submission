#include"fraction.h"
int f_numerator;
int f_denominator;
int f_gcd;

Fraction::Fraction(): m_numerator(1),m_denominator(1){}
Fraction::Fraction(int num,int deno):m_numerator(num),m_denominator(deno){}
Fraction::Fraction(const Fraction &ref):m_numerator(ref.m_numerator),m_denominator(ref.m_denominator){}
Fraction Fraction::operator+(const Fraction &ref){
            f_denominator=m_denominator*ref.m_denominator;
            f_numerator=((m_numerator*ref.m_denominator)+(ref.m_numerator*m_denominator));
            f_gcd=gcd(f_numerator,f_denominator);
            f_numerator=f_numerator/f_gcd;
            f_denominator=f_denominator/f_gcd;
            return Fraction(f_numerator,f_denominator);
}
Fraction Fraction::operator-(const Fraction &ref){
            f_denominator=m_denominator*ref.m_denominator;
            f_numerator=((m_numerator*ref.m_denominator)-(ref.m_numerator*m_denominator));
            f_gcd=gcd(f_numerator,f_denominator);
            f_numerator=f_numerator/f_gcd;
            f_denominator=f_denominator/f_gcd;
            return Fraction(f_numerator,f_denominator);
}
Fraction Fraction::operator*(const Fraction &ref){
            f_denominator=m_denominator*ref.m_denominator;
            f_numerator=m_numerator*ref.m_numerator;
            f_gcd=gcd(f_numerator,f_denominator);
            f_numerator=f_numerator/f_gcd;
            f_denominator=f_denominator/f_gcd;
            return Fraction(f_numerator,f_denominator);
}
Fraction Fraction::operator+(int val){
            f_denominator=m_denominator;
            f_numerator=(m_numerator)+(m_denominator*val);
            f_gcd=gcd(f_numerator,f_denominator);
            f_numerator=f_numerator/f_gcd;
            f_denominator=f_denominator/f_gcd;
            return Fraction(f_numerator,f_denominator);
}
Fraction Fraction::operator*(int val){
            f_denominator=m_denominator;
            f_numerator=m_numerator*val;
            f_gcd=gcd(f_numerator,f_denominator);
            f_numerator=f_numerator/f_gcd;
            f_denominator=f_denominator/f_gcd;
            return Fraction(f_numerator,f_denominator);
}
bool Fraction::operator==(const Fraction &ref){
            if((m_numerator/gcd(m_numerator,m_denominator) && m_denominator/gcd(m_numerator,m_denominator))==ref.m_numerator/gcd(ref.m_numerator,ref.m_denominator) && ref.m_denominator/gcd(ref.m_numerator,ref.m_denominator))
              return true;
            else
              return false;
}
int Fraction::gcd(int a, int b) {
      if (b==0)
        return a;
      return gcd(b, a%b);
    }
std::ostream& operator<<(std::ostream &rout, const Fraction &ref) {
  rout<<ref.m_numerator<<"/"<<ref.m_denominator<<"\n";
  return rout;
}
