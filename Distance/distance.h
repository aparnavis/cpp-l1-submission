#include<iostream>
#include<cmath>
class Distance{
    int m_feet;
    int m_inches;
    public:
        Distance();
        Distance(int,int);
        Distance(const Distance &ref);
        Distance operator+(const Distance &ref);
        Distance operator-(const Distance &ref);
        bool operator==(const Distance &ref);
        Distance operator++();
        Distance operator+(int);
        bool operator<(const Distance &ref);
        bool operator>(const Distance &ref);
        void check();
        friend std::ostream &operator<<(std::ostream &,const Distance&);
};