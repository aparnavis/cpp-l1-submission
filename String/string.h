#include<iostream>
class MyString
{
    int m_len;
    char* m_buf;
public:
    MyString();
    MyString(const char*);
    MyString(const MyString&);
    ~MyString();
    friend std::ostream &operator<<(std::ostream &,const MyString&);
};