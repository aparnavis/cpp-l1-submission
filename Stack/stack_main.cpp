#include"stack.h"
int main()
{
    Stack <int> s1,s2;
    s1.push(1);
    s2.push(10);
    s1.push(2);
    s2.push(20);
    std::cout<< "Pop s1: ";
    for(int i=0; i<2; i++)
        std::cout << s1.pop() << "\n";
    std::cout << "Pop s2: " ;    
    for(int i=0; i<2; i++)
       std::cout<< s2.pop() << "\n";
    Stack<char> s3,s4;
    s3.push('a');
    s3.push('b');
    s3.push('c');
    s4.push('x');
    s4.push('y');
    s4.push('z');
    s3.peek();
    std::cout << "Pop s3: "; 
    for(int i=0; i<3; i++)
       std::cout<< s3.pop() << "\n";
    std::cout << "Pop s4: ";
    for(int i=0; i<3; i++)
        std::cout<< s4.pop() << "\n";
    return 0;

}