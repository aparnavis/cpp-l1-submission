#include<iostream>

class Currency {
	private :
		int rupees;
		int paise;
		
  	public:
  		Currency(int ,int );
		Currency();
		
		Currency operator+(const Currency& );
		
		Currency operator+(int );
			
		Currency operator++();
		
		Currency& operator=(const Currency& );
		
		bool operator==(const Currency& );
		
		friend std::ostream& operator<<(std::ostream &rout ,const Currency &ref);

        void display() const;
};
