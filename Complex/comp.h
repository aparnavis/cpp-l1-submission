#include<iostream>


template<typename T>
class complex{
	T cr;
	T ci;
	
	public :
		complex(T x,T y) : cr(x),ci(y) {}
		complex() : cr(0),ci(0) {}
		
	complex operator+(const complex<T>& ref){
		T c_r;
		T c_i;
		c_r = cr + ref.cr;
		c_i = ci + ref.ci;
		return complex(c_r,c_i);
	}
	
	complex operator-(const complex<T>& ref){
		T c_r;
		T c_i;
		c_r = cr - ref.cr;
		c_i = ci - ref.ci;
		return complex(c_r,c_i);
	}
	
	complex operator*(const complex<T>& ref){
		T c_r;
		T c_i;
		c_r = cr * ref.cr;
		c_i = ci * ref.ci;
		return complex(c_r,c_i);
	}
	
	bool operator==(const complex<T>& ref){
		return cr == ref.cr && ci == ref.ci;
	}
	
	void display() const{
	std::cout<<cr<<"+j"<<ci<<"\n";
	}
};

