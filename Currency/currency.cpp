#include "currency.h"

Currency::Currency(int r,int p):rupees(r),paise(p) {}
Currency::Currency():rupees(0),paise(0) {}

Currency Currency::operator+(const Currency& ref) {
			int c_r;
			int c_p;
			c_r = rupees + ref.rupees;
			c_p = paise + ref.paise;
			if(c_p>100)
			{
				c_p-=100;
				c_r++;
			}
			return Currency(c_r,c_p);
		}
		
Currency Currency::operator+(int pai){
			int c_r;
			int c_p;
			c_r=rupees;
            c_p=paise + pai;
			if(c_p>100)
			{
				c_p-=100;
				c_r++;
			}
			return Currency(c_r,c_p);
		}
		
Currency Currency :: operator++()
		{
			++paise;;
			return *this;
		}
		
Currency& Currency :: operator=(const Currency &ref)
		{
			if(this == &ref) return *this;
			rupees=ref.rupees;
			paise=ref.paise;
			return *this;
		}
		
bool Currency :: operator==(const Currency& ref)
		{
			return rupees==ref.rupees && paise==ref.paise;
		}
		
std::ostream& operator<<(std::ostream &rout,const Currency &ref)
		{
			rout<<"Rs "<<ref.rupees<<":"<<ref.paise<<std::endl;
			return rout;
		}
		
void Currency :: display() const {
		std::cout<<"Rs "<<rupees<<":"<<paise<<std::endl;
		}
