#include"string.h"
#include<cstring>
MyString::MyString():m_len(0),m_buf(nullptr){
}
MyString::MyString(const char * buf)
{
    m_len=strlen(buf);
    m_buf=new char(m_len+1);
    strcpy(m_buf,buf);
}
MyString::MyString(const MyString& ref):m_len(ref.m_len)
{
    m_buf=new char(m_len+1);
    strcpy(m_buf,ref.m_buf);
}
MyString::~MyString()
{
    if(m_len>0)
    {
        delete[] m_buf;
        m_buf=NULL;
    }
}
std::ostream &operator<<(std::ostream &rout,const MyString&ref)
 {
    rout << ref.m_buf<< "\n";
    return rout;

 }
